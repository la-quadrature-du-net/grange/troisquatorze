# troisquatorze

Template wordpress à cloner pour créer des thèmes personnalisés. Ce projet a pour vocation
d'être cloné pour permettre un démarrage facile du developpement de thèmes wordpress.

Une fois forké depuis l'interface gitlab, il faut penser à modifier le nouveau thème pour
en changer le nom et le transférer au groupe /la-quadrature-du-net/grange/ avec tout
ce qui est lié à Wordpress.

Ne pas hésiter à se référer au [Codex](https://codex.wordpress.org/) pour
toute info sur le développement de thèmes (ou le fonctionnement de
Wordpress de manière générale), en particulier la page sur le
[développement de thèmes](https://codex.wordpress.org/Theme_Development)

Le répertoire scripts/ contient quelques utilitaires nécessaire à la mise
en ligne du site ou à réaliser quelques opérations de maintenance. Il
s'agit essentiellement de scripts bash.

# Installation d'un nouveau site

Pour tester le thème, le plus simple est de créer un nouveau site sur la
grange. Pour se faciliter la vie, l'URL de test sera [https://<nom du
theme>.unegrange.dev.lqdn.fr]().

Un fichier site.yml contenant les variables nécessaire au fonctionnement
du site — incluant notamment la liste des plugins — doit être rempli. Les
variables nécessaires et leur significations sont détaillées ci-dessous.

Attention, lorsque ce fichier est mis à jour sur la branche preprod du
gitlab, un déploiement en test aura lieu, ainsi que le lancement de tests.
Une mise à jour dans la branche master amènera à un déploiement en
production.

