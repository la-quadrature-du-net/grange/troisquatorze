/* 
 * index.php
 * Licence GNU General Public Licence v3 or more
 * https://www.gnu.org/licenses/gpl.html
 *
 * C'est le seul fichier obligatoire pour la création d'un thème.
 * Pour d'autres fichiers / template, se référer à la documentation
 * officielle du codex wordpress: https://codex.wordpress.org/Theme_Development
 *
 * Penser notamment à valider la checklist des templates
 */
